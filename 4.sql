SELECT o.name
FROM object o
LEFT JOIN type t ON t.id = o.type_id
WHERE t.name IS NULL;