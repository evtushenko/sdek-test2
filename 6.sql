SELECT t.id, t.name
FROM type t
JOIN object ON o.type_id = t.id
GROUP BY t.id;