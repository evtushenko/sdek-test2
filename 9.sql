SELECT t.name type, o.name object
FROM object o
LEFT JOIN type t ON t.id = o.type_id
GROUP BY o.type_id;