SELECT t.name, COUNT(o.id)
FROM type t
JOIN object ON o.type_id = t.id
GROUP BY t.id
HAVING COUNT(o.id) > 1;