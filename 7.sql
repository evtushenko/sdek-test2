SELECT t.id, t.name
FROM type t
LEFT JOIN object ON o.type_id = t.id
WHERE o.name IS NULL
GROUP BY t.id;